This directory contains various simulations for bandicoot to compare with other
libraries (specifically, Julia, TensorFlow, and PyTorch).

To download the data, go to https://www.ratml.org/datasets/bandicoot/ and
download the appropriate files and put them in the directory before you run the
`run_simulations.sh` script.

To run the scripts, `cd` to the directory containing the simulation you're
interested in, then run as `../run_simulations.sh`.  (Be prepared to debug.)
