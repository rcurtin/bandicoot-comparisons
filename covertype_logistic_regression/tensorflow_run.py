# Implement a logistic regression training on the MNIST dataset.
#
# This is based very heavily on an example TensorFlow notebook:
# https://github.com/aymericdamien/TensorFlow-Examples/blob/master/tensorflow_v2/notebooks/2_BasicModels/logistic_regression.ipynb
#
# However, we have modified this implementation be only two-class, so it is
# comparable with our bandicoot example.

from __future__ import absolute_import, division, print_function

import tensorflow as tf
import numpy as np
import time
import sys

if len(sys.argv) != 2:
  print("Batch size must be specified as the only parameter!")
  exit()

# covertype dataset parameters.
num_classes = 2
num_features = 54

# Training parameters.
learning_rate = 3e-12
epochs = 50
batch_size = int(sys.argv[1])

# Prepare data.
x = np.genfromtxt('covertype.train.csv', delimiter=',')
y = np.genfromtxt('covertype.train.labels.csv', delimiter=',')
xt = np.genfromtxt('covertype.test.csv', delimiter=',')
yt = np.genfromtxt('covertype.test.labels.csv', delimiter=',')

# Convert to float64.
x = np.array(x, np.float64)
y = np.array(y, np.long)
xt = np.array(xt, np.float64)
yt = np.array(yt, np.long)

num_steps = int(len(y) * epochs / batch_size)

# Use tf.data API to batch data.  Note that we don't shuffle to match the other
# simulations!
train_data = tf.data.Dataset.from_tensor_slices((x, y))
train_data = train_data.repeat().batch(batch_size).prefetch(1)

# Weight of shape [784, 2], the 28*28 image features, and total number of
# classes.
W = tf.Variable(tf.ones([num_features, num_classes], dtype=tf.float64), \
    name="weight", dtype=tf.float64)
# Bias of shape [2], the total number of classes.
b = tf.Variable(tf.zeros([num_classes], dtype=tf.float64), name="bias", \
    dtype=tf.float64)

# Logistic regression (Wx + b).
def logistic_regression(x):
    # Apply softmax to normalize the logits to a probability distribution.
    return tf.nn.softmax(tf.matmul(x, W) + b)

# Cross-Entropy loss function.
def cross_entropy(y_pred, y_true):
    # Encode label to a one hot vector.
    y_true = tf.one_hot(y_true, depth=num_classes, dtype=tf.float64)
    # Clip prediction values to avoid log(0) error.
    y_pred = tf.clip_by_value(y_pred, 1e-9, 1.)
    # Compute cross-entropy.
    return tf.reduce_mean(-tf.reduce_sum(y_true * tf.math.log(y_pred), 1))

# Accuracy metric.
def accuracy(y_pred, y_true):
    # Predicted class is the index of highest score in prediction vector (i.e.
    # argmax).
    correct_prediction = tf.equal(tf.argmax(y_pred, 1), \
        tf.cast(y_true, tf.int64))
    return tf.reduce_mean(tf.cast(correct_prediction, tf.float64))

# Stochastic gradient descent optimizer.
optimizer = tf.optimizers.SGD(learning_rate)

# Optimization process.
def run_optimization(x, y):
    # Wrap computation inside a GradientTape for automatic differentiation.
    with tf.GradientTape() as g:
        pred = logistic_regression(x)
        loss = cross_entropy(pred, y)

    # Compute gradients.
    gradients = g.gradient(loss, [W, b])

    # Update W and b following gradients.
    optimizer.apply_gradients(zip(gradients, [W, b]))

# Run training for the given number of steps.
start = time.perf_counter()
for step, (batch_x, batch_y) in enumerate(train_data.take(num_steps), 1):
    # Run the optimization to update W and b values.
    run_optimization(batch_x, batch_y)
end = time.perf_counter()
print(f"Training took {end - start:0.6f} seconds.")

# Test model on validation set.
pred = logistic_regression(xt)
print("Test accuracy: %f." % (accuracy(pred, yt) * 100.0))
