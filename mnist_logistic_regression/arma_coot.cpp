/**
 * @file arma_coot.cpp
 * @author Ryan Curtin
 *
 * Implement a logistic regression optimization for the MNIST dataset (just to
 * detect whether something is {4, 9}, because we need it to be two-class).
 */
#define COOT_USE_U64S64
#define ENS_PRINT_INFO

#include <ensmallen.hpp>

using namespace ens;
using namespace ens::test;

int main(int argc, char** argv)
{
  if (argc != 2)
  {
    std::cout << "Usage: arma_coot <batchSize>" << std::endl;
    exit(1);
  }

  // Parameters for learning.
  const double stepSize = 1e-8;
  const size_t numEpochs = 50;
  const double lambda = 0.5;
  const size_t batchSize = (size_t) atoi(argv[1]);

  coot::get_rt().init(true);
  coot::get_rt().cuda_rt.init(false, 0, 0, true);

  // All preprocessing should be done beforehand.
  arma::mat mnistTrain, mnistTest;
  arma::Col<size_t> mnistTrainLabelsT, mnistTestLabelsT;

  mnistTrain.load("mnist_train.csv");
  mnistTest.load("mnist_test.csv");
  mnistTrainLabelsT.load("mnist_train_labels.csv");
  mnistTestLabelsT.load("mnist_test_labels.csv");

  mnistTrain = mnistTrain.t();
  mnistTest = mnistTest.t();
  arma::Row<size_t> mnistTrainLabels = mnistTrainLabelsT.t();
  arma::Row<size_t> mnistTestLabels = mnistTestLabelsT.t();

  StandardSGD sgd;
  sgd.StepSize() = stepSize;
  sgd.Shuffle() = false; // shuffling is not yet implemented in bandicoot!
  sgd.BatchSize() = batchSize;
  sgd.MaxIterations() = numEpochs * mnistTrain.n_cols;

  arma::wall_clock c;

  // First, let's do it on the CPU.
  LogisticRegressionFunction<arma::mat, arma::Row<size_t>> lrArma(mnistTrain,
      mnistTrainLabels, lambda);
  arma::mat coordinates = lrArma.GetInitialPoint();

  c.tic();
  sgd.Optimize(lrArma, coordinates);
  const double armaTime = c.toc();

  // Now compute the accuracy on the test set.
  const double accuracy = lrArma.ComputeAccuracy(mnistTest, mnistTestLabels,
      coordinates);

  // Next, let's do it with the OpenCL backend.
  double clTime, clAccuracy;
  {
    coot::get_rt().backend = coot::CL_BACKEND;
    coot::mat mnistTrainGPU(mnistTrain);
    coot::mat mnistTestGPU(mnistTest);
    coot::Row<size_t> mnistTrainLabelsGPU(mnistTrainLabels);
    coot::Row<size_t> mnistTestLabelsGPU(mnistTestLabels);

    LogisticRegressionFunction<coot::mat, coot::Row<size_t>>
        lrCoot1(mnistTrainGPU, mnistTrainLabelsGPU, lambda);
    coot::mat coordinatesGPU = lrCoot1.GetInitialPoint();

    c.tic();
    sgd.Optimize(lrCoot1, coordinatesGPU);
    clTime = c.toc();

    clAccuracy = lrCoot1.ComputeAccuracy(mnistTestGPU, mnistTestLabelsGPU,
        coordinatesGPU);
  }

  // Finally, do it with CUDA.
  double cudaTime, cudaAccuracy;
  {
    coot::get_rt().backend = coot::CUDA_BACKEND;

    coot::mat mnistTrainGPU(mnistTrain);
    coot::mat mnistTestGPU(mnistTest);
    coot::Row<size_t> mnistTrainLabelsGPU(mnistTrainLabels);
    coot::Row<size_t> mnistTestLabelsGPU(mnistTestLabels);

    LogisticRegressionFunction<coot::mat, coot::Row<size_t>>
        lrCoot2(mnistTrainGPU, mnistTrainLabelsGPU, lambda);
    coot::mat coordinatesGPU = lrCoot2.GetInitialPoint();

    StandardSGD sgd2;
    sgd2.StepSize() = stepSize;
    sgd2.Shuffle() = false; // shuffling is not yet implemented in bandicoot!
    sgd2.BatchSize() = batchSize;
    sgd2.MaxIterations() = numEpochs * mnistTrain.n_cols;

    c.tic();
    sgd2.Optimize(lrCoot2, coordinatesGPU);
    cudaTime = c.toc();

    cudaAccuracy = lrCoot2.ComputeAccuracy(mnistTestGPU, mnistTestLabelsGPU,
        coordinatesGPU);
  }

  std::cout << "Armadillo:          " << armaTime << "s training, "
      << accuracy << "% accuracy on test set." << std::endl;
  std::cout << "Bandicoot (OpenCL): " << clTime << "s training, "
      << accuracy << "% accuracy on test set." << std::endl;
  std::cout << "Bandicoot (CUDA):   " << cudaTime << "s training, "
      << accuracy << "% accuracy on test set." << std::endl;
}
