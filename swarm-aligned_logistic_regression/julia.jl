using CUDA
using CSV
using DataFrames
using BenchmarkTools: @benchmark
using LinearAlgebra

# This is basically a transliteration of the ensmallen implementation.
function lr_eval_and_gradient!(params,
                               X,
                               y,
                               gradient,
                               lambda::Float64,
                               firstPoint::Int,
                               lastPoint::Int)

  batchSize = lastPoint - firstPoint
  regularization = lambda .* params[2:end] / size(X)[2] * batchSize
  objectiveRegularization = lambda * batchSize / (2 * size(X)[2]) *
      dot(params[2:end], params[2:end])

  sigmoids = 1.0 ./ (1.0 .+ exp.(-(params[1:1] .+ params[2:end]' *
      X[:, firstPoint:lastPoint])))
#  print("sigmoids: $(sigmoids[1:20])\n")

  gradient[1:1] = -sum(y[firstPoint:lastPoint] .- sigmoids')
  gradient[2:end] = X[:, firstPoint:lastPoint] *
      (sigmoids' .- y[firstPoint:lastPoint]) .+ regularization

  result = objectiveRegularization - sum(log.(1.0 .- y[firstPoint:lastPoint] .+
      sigmoids' .* (2 * y[firstPoint:lastPoint] .- 1.0)))

  return result
end

# Optimize and update params.
function optimize!(params, X, y, lambda, stepSize, batchSize, epochs)
#  params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)

  # Initialize space for the gradient.
  gradient = CuArray{Float64}(undef, size(params))
  numBatches = ceil(size(X, 2) / batchSize)

  for epoch in 1:epochs
    obj = 0.0
    for batch in 1:numBatches
      # ugh, 1-indexing...
      firstPoint = convert(Int, (batch - 1) * batchSize + 1)
      lastPoint = convert(Int, min(size(X, 2) - 1, firstPoint + batchSize - 1))
      obj += lr_eval_and_gradient!(params, X, y, gradient, lambda, firstPoint,
          lastPoint)

      params[:] -= stepSize * gradient
    end
    print("Epoch $(epoch): objective $(obj).\n")
  end

  return nothing
end

function compute_accuracy(params, data, labels)::Float64
    predictions = floor.((1.0 ./ (1.0 .+ exp.(-(params[1:1] .+ params[2:end]' *
        data)))) .+ 0.5)

    return sum(predictions' .== labels) / length(labels)
end

# Load data from CSV.
swarmTrain = CSV.read("aligned.csv", DataFrame, header=false)
swarmTrainLabels = CSV.read("aligned.labels.csv", DataFrame, header=false)

# Transfer the data to the GPU.  Holy crap the convert() drives me nuts.
swarmTrainGPU = CuArray{Float64}(convert(Matrix, swarmTrain)')
swarmTrainLabelsGPU = CuArray{Int64}(convert(Matrix, swarmTrainLabels))

params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)

stepSize = 1e-12
numEpochs = 50
lambda = 0.5
batchSize = 128 # Change this manually...

print("Using batch size $(batchSize)...\n")

# Now benchmark the optimization.
optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)

print("Accuracy of model: $(100 * compute_accuracy(params, swarmTrainGPU,
      swarmTrainLabelsGPU))%.\n")

batchSize = 1024

print("Using batch size $(batchSize)...\n")

params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)
params = CUDA.zeros(Float64, size(swarmTrain, 2) + 1)
@time optimize!(params, swarmTrainGPU, swarmTrainLabelsGPU, lambda, stepSize,
    batchSize, numEpochs)

# Compute accuracy.
print("Accuracy of model: $(100 * compute_accuracy(params, swarmTrainGPU,
      swarmTrainLabelsGPU))%.\n")
