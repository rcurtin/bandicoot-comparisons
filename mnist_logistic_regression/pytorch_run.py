import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import time
import sys
from torch.autograd import Variable

if len(sys.argv) != 2:
  print("Must provide batch size as the only parameter.")
  exit()

# Hyper-parameters
input_size = 28 * 28    # 784
num_classes = 2
num_epochs = 50
batch_size = int(sys.argv[1])
learning_rate = 1e-8 * 60000

# MNIST dataset (images and labels)
train_dataset = torchvision.datasets.MNIST(root='../../data',
                                           train=True,
                                           transform=transforms.ToTensor(),
                                           download=True)

test_dataset = torchvision.datasets.MNIST(root='../../data',
                                          train=False,
                                          transform=transforms.ToTensor())

# Convert to a two-class problem.
for i in range(len(train_dataset.targets)):
  if train_dataset.targets[i] == 4 or train_dataset.targets[i] == 9:
    train_dataset.targets[i] = 1
  else:
    train_dataset.targets[i] = 0

for i in range(len(test_dataset.targets)):
  if test_dataset.targets[i] == 4 or test_dataset.targets[i] == 9:
    test_dataset.targets[i] = 1
  else:
    test_dataset.targets[i] = 0

device = torch.device("cuda")

train_torch = train_dataset.data.type(torch.DoubleTensor).to(device)
train_torch_labels = train_dataset.targets.type(torch.LongTensor).to(device)
test_torch = test_dataset.data.type(torch.DoubleTensor).to(device)
test_torch_labels = test_dataset.targets.type(torch.LongTensor).to(device)

# Data loader (input pipeline)
train_loader = torch.utils.data.DataLoader(
    dataset=train_torch,
    batch_size=batch_size,
    shuffle=False)
train_label_loader = torch.utils.data.DataLoader(
    dataset=train_torch_labels,
    batch_size=batch_size,
    shuffle=False)
test_loader = torch.utils.data.DataLoader(
    dataset=test_torch,
    batch_size=batch_size,
    shuffle=False)
test_label_loader = torch.utils.data.DataLoader(
    dataset=test_torch_labels,
    batch_size=batch_size,
    shuffle=False)

class LogisticRegression(torch.nn.Module):
  def __init__(self, input_dim, output_dim):
    super(LogisticRegression, self).__init__()
    self.linear = torch.nn.Linear(input_dim, output_dim)

  def forward(self, x):
    outputs = self.linear(x)
    return outputs

device = torch.device("cuda")
torch.set_default_tensor_type(torch.DoubleTensor)

torch.cuda.synchronize()

print("Created DataLoaders.")

model = LogisticRegression(input_size, num_classes).to(device)
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

print("Created model and optimizer.")

start = time.perf_counter()
for epoch in range(num_epochs):
  for (images, labels) in zip(train_loader, train_label_loader):
    optimizer.zero_grad()
    outputs = model(images.reshape(-1, input_size))
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()
  print(f"Finished epoch {epoch}.")
end = time.perf_counter()

print(f"Training took {end - start:0.6f} seconds.")

# Test the model
# In test phase, we don't need to compute gradients (for memory efficiency)
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in zip(train_loader, train_label_loader):
        images = images.reshape(-1, input_size)
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()

    print('Train accuracy: {} %'.format(100 * correct / total))

    correct = 0
    total = 0
    for images, labels in zip(test_loader, test_label_loader):
        images = images.reshape(-1, input_size)
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()

    print('Test accuracy: {} %'.format(100 * correct / total))
