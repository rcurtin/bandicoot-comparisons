#!/bin/bash
#
# Run the logistic regression simulations, printing the results in two CSV-type
# tables.  Run this from the directory containing the simulation you want to
# run!
#
# This runs with batch size 128 and 1024.
#
# The code is kinda ugly... sorry!  (I'm not really sorry.)

echo "lib, runtime128, acc128, runtime1024, acc1024";

# Armadillo and Bandicoot.
./arma_coot 128 2>/dev/null > tmp1;
./arma_coot 1024 2>/dev/null > tmp2;

arma128rt=`grep "Armadillo" tmp1 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
arma128acc=`grep "Armadillo" tmp1 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;
arma1024rt=`grep "Armadillo" tmp2 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
arma1024acc=`grep "Armadillo" tmp2 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;

echo "arma, $arma128rt, $arma128acc, $arma1024rt, $arma1024acc";

cl128rt=`grep "Bandicoot (OpenCL)" tmp1 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
cl128acc=`grep "Bandicoot (OpenCL)" tmp1 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;
cl1024rt=`grep "Bandicoot (OpenCL)" tmp2 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
cl1024acc=`grep "Bandicoot (OpenCL)" tmp2 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;

echo "coot-cl, $cl128rt, $cl128acc, $cl1024rt, $cl1024acc";

cuda128rt=`grep "Bandicoot (CUDA)" tmp1 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
cuda128acc=`grep "Bandicoot (CUDA)" tmp1 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;
cuda1024rt=`grep "Bandicoot (CUDA)" tmp2 | sed 's/.*:[ ]*\([0-9.]*\)s.*$/\1/'`;
cuda1024acc=`grep "Bandicoot (CUDA)" tmp2 | sed 's/.*, \([0-9.]*\)%.*$/\1/'`;

echo "coot-cuda, $cuda128rt, $cuda128acc, $cuda1024rt, $cuda1024acc";

rm -f tmp1 tmp2;

# Now run Julia.
julia julia.jl > tmp;

jl128rt=`grep "seconds" tmp | head -3 | awk -F' ' '{ print $1 }' | tr '\n' '+' | sed 's/+$/)\/3.0/' | sed 's/^/(/' | julia`;
jl128acc=`grep "Accuracy" tmp | head -1 | sed 's/.*: \([0-9.]*\)%.*$/\1/'`;
jl1024rt=`grep "seconds" tmp | tail -3 | awk -F' ' '{ print $1 }' | tr '\n' '+' | sed 's/+$/)\/3.0/' | sed 's/^/(/' | julia`;
jl1024acc=`grep "Accuracy" tmp | tail -1 | sed 's/.*: \([0-9.]*\)%.*$/\1/'`;

echo "julia, $jl128rt, $jl128acc, $jl1024rt, $jl1024acc";

rm -f tmp;

# Next, PyTorch.
python3 pytorch_run.py 128 > tmp1;
python3 pytorch_run.py 1024 > tmp2;

torch128rt=`grep "Training took" tmp1 | sed 's/^.* \([0-9.]*\) seconds.*$/\1/'`;
torch128acc=`grep "Test acc" tmp1 | sed 's/^.*: \([0-9.]*\) .*$/\1/'`;
torch1024rt=`grep "Training took" tmp2 | sed 's/^.* \([0-9.]*\) seconds.*$/\1/'`;
torch1024acc=`grep "Test acc" tmp2 | sed 's/^.*: \([0-9.]*\) .*$/\1/'`;

echo "pytorch, $torch128rt, $torch128acc, $torch1024rt, $torch1024acc";

# Finally, TensorFlow.
# (hide pointless status messages that are irritatingly sent to stderr)
python3 tensorflow_run.py 128 2>/dev/null 1> tmp1;
python3 tensorflow_run.py 1024 2>/dev/null 1> tmp2;

tf128rt=`grep "Training took" tmp1 | sed 's/^.* \([0-9.]*\) seconds.*$/\1/'`;
tf128acc=`grep "Test acc" tmp1 | sed 's/^.*: \([0-9.]*\)[.].*$/\1/'`;
tf1024rt=`grep "Training took" tmp2 | sed 's/^.* \([0-9.]*\) seconds.*$/\1/'`;
tf1024acc=`grep "Test acc" tmp2 | sed 's/^.*: \([0-9.]*\)[.].*$/\1/'`;

echo "tensorflow, $tf128rt, $tf128acc, $tf1024rt, $tf1024acc";

rm -f tmp1 tmp2;
