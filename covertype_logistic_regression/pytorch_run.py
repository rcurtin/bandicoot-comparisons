import numpy as np
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import time
from torch.autograd import Variable
import sys

if len(sys.argv) != 2:
  print("Batch size must be specified as the only parameter!")
  exit()

# Hyper-parameters
input_size = 54
num_classes = 2
num_epochs = 50
batch_size = int(sys.argv[1])
learning_rate = 1e-5

# MNIST dataset (images and labels)
train_dataset = np.genfromtxt('covertype.train.csv', delimiter=',')
train_labels = np.genfromtxt('covertype.train.labels.csv', delimiter=',')
test_dataset = np.genfromtxt('covertype.test.csv', delimiter=',')
test_labels = np.genfromtxt('covertype.test.labels.csv', delimiter=',')

device = torch.device("cuda")

train_torch = torch.DoubleTensor(train_dataset).to(device)
train_torch_labels = torch.LongTensor(train_labels).to(device)
test_torch = torch.DoubleTensor(test_dataset).to(device)
test_torch_labels = torch.LongTensor(test_labels).to(device)

# Data loader (input pipeline)
train_loader = torch.utils.data.DataLoader(
    dataset=train_torch,
    batch_size=batch_size,
    shuffle=False)
train_label_loader = torch.utils.data.DataLoader(
    dataset=train_torch_labels,
    batch_size=batch_size,
    shuffle=False)
test_loader = torch.utils.data.DataLoader(
    dataset=test_torch,
    batch_size=batch_size,
    shuffle=False)
test_label_loader = torch.utils.data.DataLoader(
    dataset=test_torch_labels,
    batch_size=batch_size,
    shuffle=False)

class LogisticRegression(torch.nn.Module):
  def __init__(self, input_dim, output_dim):
    super(LogisticRegression, self).__init__()
    self.linear = torch.nn.Linear(input_dim, output_dim)

  def forward(self, x):
    outputs = self.linear(x)
    return outputs

device = torch.device("cuda")
torch.set_default_tensor_type(torch.DoubleTensor)

torch.cuda.synchronize()

print("Created DataLoaders.")

model = LogisticRegression(input_size, num_classes).to(device)
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

print("Created model and optimizer.")

start = time.perf_counter()
for epoch in range(num_epochs):
  for (images, labels) in zip(train_loader, train_label_loader):
    optimizer.zero_grad()
    outputs = model(images.reshape(-1, input_size))
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()
  print(f"Finished epoch {epoch}.")
end = time.perf_counter()

print(f"Training took {end - start:0.6f} seconds.")

# Test the model
# In test phase, we don't need to compute gradients (for memory efficiency)
with torch.no_grad():
    correct = 0
    total = 0
    for images, labels in zip(test_loader, test_label_loader):
        images = images.reshape(-1, input_size)
        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()

    print('Test accuracy: {} %'.format(100 * correct / total))
